package com.supermap.desktop.netservices.iserver;

/**
 * 发布 iServer 服务的各种响应结果的 Key值
 * 
 * @author highsad
 *
 */
public class ResonseKey {

	public class CreateUpload {
		public static final String SUCCESS = "succeed";
		public static final String POST_RESULT_TYPE = "postResultType";
		public static final String NEW_RESOURCE_LOCATION = "newResourceLocation";
		public static final String NEW_RESOURCE_ID = "newResourceID";
	}
}
